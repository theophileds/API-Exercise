import os

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from app import app
from db import db
from populate_db import PopulateDb

app.config.from_object(os.getenv('APP_SETTINGS', 'config.DevelopmentConfig'))
db.init_app(app)

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)
manager.add_command('populate', PopulateDb(app))

if __name__ == '__main__':
    manager.run()
