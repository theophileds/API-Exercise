# Introduction
Hi and welcome to the API-Exercise work achievement.
 
## Application Description
The application has been written with flask-restful, which a more straightforward library to code RestFul API than Flask!

The application need a Postgres database for storage purposes, and is optimized for container usage.
In this documenation I'll explain how to run, build, test and finally deploy this application to a Kubernetes cluster. I'll also add a final note regarding the ways of improvements.

## Running instructions
The application can be runned locally

You need to have a Postgres instance available then create a people db with uuid-ossp extension installed.

    docker run -p 5432:5432 --name spring_postgres -e POSTGRES_PASSWORD=password -d postgres:12.0
    docker run -it --rm --link spring_postgres:postgres postgres psql -h postgres -U postgres
    create database people;
    \c people;
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

To prepare the Python environment, you need to install pipenv

    pip install pipenv
    pipenv install
    pipenv shell
Then you can launch the migration script to create the tables

    python run.py db upgrade

Eventually you can fill the database from the csv with the populate script

    python run.py populate

Finally you can run the application

    python run.py runserver

## How to test your application
You can test every endpoints using curl commands, here are few examples

GET /people

    curl localhost:5000/people -H "Content-Type: application/json"

POST /people

    curl -XPOST -d '{ "survived": true, "passengerClass": 3, "name": "Mr. Owen Harris Braund", "sex": "male", "age": 22, "siblingsOrSpousesAboard": 1, "parentsOrChildrenAboard":0, "fare":7.25}' localhost:5000/people -H "Content-Type: application/json"

GET /people/*uuid*

    curl localhost:5000/people/146761ff-336a-4f31-8070-9db6ea55ba59 -H "Content-Type: application/json"

PUT /people/*uuid*

    curl -XPUT -d '{ "survived": false, "passengerClass": 4, "name": "Mr. Owen Harris Braund", "sex": "male", "age": 22, "siblingsOrSpousesAboard": 1, "parentsOrChildrenAboard":0, "fare":7.25}' localhost:5000/people/146761ff-336a-4f31-8070-9db6ea55ba59 -H "Content-Type: application/json"

DELETE /people/*uuid*

    curl -XDELETE localhost:5000/people/146761ff-336a-4f31-8070-9db6ea55ba59 -H "Content-Type: application/json"

## Build your application
If you don't have Python installed, no worries, you can emulate a dev environment by using docker-compose
Navigate to the end_to_end directory

    cd end_to_end

Then you can launch Postgres and flask

    docker-compose up --build -d

You'll still need to migrate the database

    docker-compose exec flask /venv/bin/python run.py db upgrade

And eventually populate it

    docker-compose exec flask /venv/bin/python run.py populate

You can still run tests the same way as if you were running the application locally.
Finally you should close the stack when you're done

    docker-compose down

## Deploy instructions

You'll need Docker, and a Kubernetes environment and a containers registry.
In the Flask Applicaiton manifest file, I have voluntarily left the address of the image I previously built in my register hosted on Google Cloud. You can still use it!

In my example I've been using Google Kubernetes Engine and Google Container Registry.
Feel free to use your own environment

To build a Docker image and push it yo Kubernetes

    docker build . -t theo/flask-app
    docker tag theo/flask-app eu.gcr.io/theophile/flask-app:0.2
    docker push eu.gcr.io/theophile/flask-app:0.2

I've been using Helm to deploy Postgres, so you'll probably need to initialize it before

    cd infrastructure
    kubectl create -f rbac-config.yaml
    helm init --service-account tiller --history-max 200

You'll need to deploy Postgres before as Flask will need the gather the secrets from it!

    helm install --name postgres \
             --set image.repository=postgres \
             --set postgresqlDatabase=people \
             --set postgresqlUsername=people \
             --set postgresqlPassword=q6WKgrKMfNuhdaEX \
             --set image.tag=12.0 \
             --set postgresqlDataDir=/data/pgdata \
             --set persistence.mountPath=/data/ \
             stable/postgresql

I've been struggling to automate the initialization script of Postgres to install the uuid-ossp extension as I did with docker-compose, so unfortunatly you'll need to set it manually.

> You'll need to do it only one time as long as you don't delete the volume storage!

    export POSTGRES_PASSWORD=$(kubectl get secret --namespace default postgres-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
    kubectl run postgres-postgresql-client --rm --tty -i --restart='Never' --namespace default \
        --image docker.io/postgres:12.0 \
        --env="PGPASSWORD=$POSTGRES_PASSWORD" \
        --command -- psql --host postgres-postgresql -U people -d people -p 5432 \
        -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'


Then you can deploy the flask app

    kubectl apply -f flask-app.yml

You'll also need to run the migration script

    FLASK_POD_NAME=$(kubectl get pod -l app=flask -o jsonpath="{.items[0].metadata.name}")
    kubectl exec $FLASK_POD_NAME --stdin --tty -- /venv/bin/python run.py db upgrade

Finally you can populate the database

    kubectl exec $FLASK_POD_NAME --stdin --tty -- /venv/bin/python run.py populate

## Futher improvements
The application have zero security, a good way to enhance it would be to implement a token mecanism to authenticate users and ideally using a third tier party such as Auth0
Also the ingress rule in Kubernetes is on HTTP, a good way should be to move toward HTTPS 