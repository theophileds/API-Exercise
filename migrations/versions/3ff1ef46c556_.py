"""empty message

Revision ID: 3ff1ef46c556
Revises: 
Create Date: 2019-10-26 13:20:52.339055

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '3ff1ef46c556'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('people',
    sa.Column('uuid', postgresql.UUID(as_uuid=True), server_default=sa.text('uuid_generate_v4()'), nullable=False),
    sa.Column('survived', sa.Boolean(), nullable=True),
    sa.Column('passenger_class', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=256), nullable=True),
    sa.Column('sex', sa.String(), nullable=True),
    sa.Column('age', sa.Integer(), nullable=True),
    sa.Column('siblings_or_spouses_aboard', sa.Integer(), nullable=True),
    sa.Column('parents_or_children_aboard', sa.Integer(), nullable=True),
    sa.Column('fare', sa.Float(), nullable=True),
    sa.PrimaryKeyConstraint('uuid')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('people')
    # ### end Alembic commands ###
