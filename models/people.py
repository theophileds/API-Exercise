import uuid as uuid_validator

from sqlalchemy import text as sa_text
from sqlalchemy.dialects.postgresql import UUID

from db import db


class PeopleModel(db.Model):
    __tablename__ = 'people'

    uuid = db.Column(UUID(as_uuid=True), primary_key=True, server_default=sa_text("uuid_generate_v4()"))
    survived = db.Column(db.Boolean)
    passenger_class = db.Column(db.Integer)
    name = db.Column(db.String(256))
    sex = db.Column(db.String())
    age = db.Column(db.Integer)
    siblings_or_spouses_aboard = db.Column(db.Integer)
    parents_or_children_aboard = db.Column(db.Integer)
    fare = db.Column(db.Float)

    def __init__(self, survived, passenger_class, name, sex, age, siblings_or_spouses, parents_or_children, fare):
        self.survived = survived
        self.passenger_class = passenger_class
        self.name = name
        self.sex = sex
        self.age = age
        self.siblings_or_spouses_aboard = siblings_or_spouses
        self.parents_or_children_aboard = parents_or_children
        self.fare = fare

    def json(self):
        return {
            'uuid': str(self.uuid),
            'survived': self.survived,
            'passengerClass': self.passenger_class,
            'name': self.name,
            'sex': self.sex,
            'age': self.age,
            'siblingsOrSpousesAboard': self.siblings_or_spouses_aboard,
            'parentsOrChildrenAboard': self.parents_or_children_aboard,
            'fare': self.fare
        }

    @classmethod
    def find_by_uuid(cls, uuid):
        try:
            uuid_validator.UUID(uuid)
        except ValueError:
            return False
        return cls.query.filter_by(uuid=uuid).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
