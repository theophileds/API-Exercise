from flask_restful import Resource, reqparse

from models.people import PeopleModel

_people_parser = reqparse.RequestParser()
_people_parser.add_argument('survived', type=bool, required=False)
_people_parser.add_argument('passengerClass', type=int, required=False, dest="passenger_class")
_people_parser.add_argument('name', type=str, required=True, help="This field cannot be blank.")
_people_parser.add_argument('sex', type=str, required=False)
_people_parser.add_argument('age', type=int, required=False)
_people_parser.add_argument('siblingsOrSpousesAboard', type=int, required=False, dest="siblings_or_spouses")
_people_parser.add_argument('parentsOrChildrenAboard', type=int, required=False, dest="parents_or_children")
_people_parser.add_argument('fare', type=float, required=False)


class People(Resource):
    def get(self, uuid):
        people = PeopleModel.find_by_uuid(uuid)
        if people:
            return people.json()
        return {'message': 'People not found'}, 404

    def delete(self, uuid):
        people = PeopleModel.find_by_uuid(uuid)
        if people:
            people.delete_from_db()
            return {'message': 'People deleted.'}
        return {'message': 'People not found.'}, 404

    def put(self, uuid):
        args = _people_parser.parse_args()
        people = PeopleModel.find_by_uuid(uuid)

        if people:
            people.survived = args['survived']
            people.passenger_class = args['passenger_class']
            people.name = args['name']
            people.sex = args['sex']
            people.age = args['age']
            people.siblings_or_spouses_aboard = args['siblings_or_spouses']
            people.parents_or_children_aboard = args['parents_or_children']
            people.fare = args['fare']
        else:
            people = PeopleModel(**args)

        people.save_to_db()

        return people.json()


class PeopleList(Resource):
    def get(self):
        peoples = list(map(lambda x: x.json(), PeopleModel.find_all()))
        return peoples, 200

    def post(self):
        args = _people_parser.parse_args()
        people = PeopleModel(**args)

        try:
            people.save_to_db()
        except:
            return {"message": "An error occurred inserting People."}, 500

        return people.json(), 201
