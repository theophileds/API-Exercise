import os

from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from resources.people import People, PeopleList

app = Flask(__name__)
app.config.from_object(os.getenv('APP_SETTINGS', 'config.DevelopmentConfig'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
api = Api(app)

api.add_resource(People, '/people/<string:uuid>')
api.add_resource(PeopleList, '/people')

if __name__ == '__main__':
    from db import db

    db.init_app(app)
    app.run()
