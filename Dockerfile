FROM python:3.7-slim AS compile-image

MAINTAINER theophileds@gmail.com

WORKDIR /tmp

COPY Pipfile* ./

RUN apt-get update && apt-get install -y libpq-dev build-essential

RUN pip install pipenv \
    && pipenv lock --requirements > requirements.txt \
    && python -m venv /venv \
    && /venv/bin/pip install -r requirements.txt

FROM python:3.7-slim AS build-image

WORKDIR /usr/src/app

COPY --from=compile-image /venv /venv

COPY . ./

RUN apt-get update && apt-get install -y libpq-dev

EXPOSE 5000

ENTRYPOINT [ "/venv/bin/python", "run.py" ]

CMD [ "runserver", "--host", "0.0.0.0" ]