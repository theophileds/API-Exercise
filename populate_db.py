import pandas as pd
from flask_script import Command
from sqlalchemy import create_engine

from models.people import PeopleModel


class PopulateDb(Command):
    def __init__(self, app):
        self.app = app

    def run(self):
        engine = create_engine(self.app.config['SQLALCHEMY_DATABASE_URI'])
        file_name = 'titanic.csv'
        df = pd.read_csv(file_name)
        df['survived'] = df['survived'] > 0
        df.to_sql(con=engine, index=False, name=PeopleModel.__tablename__, if_exists='append')
